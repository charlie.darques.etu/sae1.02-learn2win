Learn2Win
===========

Développé par Simon Lietard et Charlie Darques
Contacts : <simon.lietard.etu@univ-lille.fr> , <charlie.darques.etu@univ-lille.fr>

# Présentation de Learn2Win

Learn2Win est un jeu pédagogique qui met en scène un combattant affrontant des ennemis grâce à ses connaissances. Chaque niveau du jeu est construit de manière à ce que le joueur s'entraîne à répondre à des questions sur différents sujets, pour qu'il puisse affronter chaque ennemi ensuite. Le jeu vient de l'idée que le savoir et la connaissance sont des forces, et il nous semble important que les enfants puissent en avoir conscience afin de croire en eux-mêmes et en l'intérêt d'apprendre. 

Des captures d'écran illustrant le fonctionnement du logiciel sont proposées dans le répertoire shots.


# Utilisation de Learn2Win

Afin d'utiliser le projet, il suffit de taper les commandes suivantes dans un terminal :

```
./compile.sh
```
Permet la compilation des fichiers

```
./run.sh
```
Permet le lancement du jeu