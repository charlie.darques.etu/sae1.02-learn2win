import extensions.File;
import extensions.CSVFile;

class Learn2Win extends Program{
    // Variables de base
    
    final String lineBreak = "\n";
    int nbDeTour = 0;
    
//--------------------------------------------------------------------------
    
    // Fonction de class (pour crée un objet de la classe)

    // Créer un nouveau joueur
    Joueur newJoueur(String nom, int nbVies, int ptsAttaque){
        Joueur j = new Joueur();
        j.nom = nom;
        j.nbVies = 3;
        j.ptsAttaque = 30;
        return j;
    }

    // Créer un nouvel ennemi
    Ennemi newEnnemi(int ptsVie, int scoreFrancaisRequis, int scoreGeoRequis, int scoreMathsRequis) {
        Ennemi e = new Ennemi();
        e.ptsVie = ptsVie;
        e.scoreFrancaisRequis = scoreFrancaisRequis;
        e.scoreGeoRequis = scoreGeoRequis;
        e.scoreMathsRequis = scoreMathsRequis;
        return e;
    }

// -----------------------------------------------------------------------------------------------
    // créer un nouveau joueur
    Joueur creerJoueur () {
        Joueur j = newJoueur("player", 3, 30);
        return j;
    }
    
    String pseudoJoueur(Joueur j) {
        println("Entrez votre pseudo : ");
        String pseudo = readString();
        j.nom = pseudo;
        return j.nom;
    }

    // Charger les fichiers dans un tableau de CSVFile 
    CSVFile [] chargerFichiersCsv(String directory) {
        // donne le nom du fichier et son contenu
        String [] filenoms = getAllFilesFromDirectory(directory);
        CSVFile [] filesTab = new CSVFile [length(filenoms)];

        for (int idx = 0; idx < length(filesTab); idx++) {
            filesTab[idx] = loadCSV("data/"+filenoms[idx]);
        }
        return filesTab;
    }

    // afficher les tableaux de dessins avec le répertoire et le nom du fichier
    String chargerFichierTexte(String directory, String nomFichier) {
        String res = "";
        String [] ressources = getAllFilesFromDirectory(directory);
        

        for (int i = 0; i < length(ressources); i++) {
            if (equals(ressources[i], nomFichier)) {

                File file = newFile(directory + "/" + nomFichier);
                for (int cpt = 0; cpt < 16; cpt ++) {
                    res = res + readLine(file) + "\n";
                }
            }
        }
        return res;
    }
    
    // Pour envoyer la matière 
    int choixMatieres(int nbDeTour){
        
        String [] filenoms = getAllFilesFromDirectory("data");
        String nomMatiere = ""; String name = "";
        int maths = 0;int geo = 0;int francais = -1;

        for(int idx = 0; idx < length(filenoms); idx++){
            name = filenoms[idx];
            if(equals(name, "francais.csv")){
                francais = idx;
            }
            else if(equals(name, "geo.csv")){
                geo = idx;
            }    
            else if(equals(name, "maths.csv")){
                maths = idx;
            }
            
        }
        int choix = -1;
        int i = 0;

        while(choix == -1){
            if(nbDeTour == i){
                nomMatiere = "Français";
                choix = francais; 
            }else if(nbDeTour == 1+i){
                nomMatiere = "Géographie";
                choix = geo;
            }    
            else if((nbDeTour == 2+i)){
                nomMatiere = "Mathématiques";
                choix = maths;
            }
            i = i + 3; 
        }
        // Pour envoyer la matière
        println("--------->" + lineBreak);
        println("Vous aller affronter la matière : " + nomMatiere);
        println("--------->" + lineBreak);
        return choix;
    } 

    // Pose une question d'une matière sélectionnée 
    boolean poserQuestions(int num_matieres, CSVFile [] dataCsv, int num_question) {

        int nbchance = 5;
        String rep_ad = getCell(dataCsv[num_matieres], num_question, 2);
        
        println("La question est :" );
        print(getCell(dataCsv[num_matieres], num_question, 1) + lineBreak);
        println("Entrer la réponse :" + lineBreak);

        String rep_utilisateur = toLowerCase(readString());

        for(int tour = 1; tour < 5; tour++){
            println(lineBreak);
            if(equals(rep_utilisateur, rep_ad)){
                println(ANSI_GREEN + "Bonne réponse !" + " Vous avez trouvé en " + tour + " tour(s)."+ lineBreak);
                return true;
            }
            else{
                print(ANSI_RED + "Ce n'est pas la bonne réponse, attention ! " + lineBreak +"Il vous reste " + (nbchance-tour) + " chance(s)." + ANSI_RESET + lineBreak);
            }
            rep_utilisateur = toLowerCase(readString());
        }
        println(ANSI_RED + "Dommage, vous n'avez pas trouvé la bonne réponse." + ANSI_RESET +lineBreak + "La réponse était : \""+ rep_ad + "\".");
        println(lineBreak +"-----------------------------------------------------" + lineBreak);
        return false;
    }

    // gestion du choix du menu principal
    int startGame(){
        String choix = readString();

        while(!(equals(choix, "1")) && !(equals(choix, "2"))){
            println("Entrez une valeur correcte ! [1 ou 2]");
            choix = readString();
        }

        int res = stringToInt(choix);
    return res;
    }

    // accueil et explications des règles au tout premier tour
    void welcome (String player, String nom){
        println(player);
        println("Vous êtes là ! Laissez moi vous expliquez la situation." + lineBreak + "Vous êtes sur le point d'entamer un grand voyage vers la connaissance." + lineBreak + lineBreak);
        println("Votre mission est d'en apprendre le plus possible sur différents sujets, car le savoir" + lineBreak + "est une force qui dépasse toutes les autres." + lineBreak + lineBreak);
        println("Mais ce ne sera pas de tout repos ! Vous allez rencontrer différents ennemis que vous" + lineBreak + "ne pourriez battre que si vous répondez à toutes leurs questions." + lineBreak + lineBreak);
        println("Pour vous entraîner, vous trouverez des coffres tout au long de votre chemin." + lineBreak + "Ouvrez les pour valider vos connaissances et gagner à coup sûr !");
        println("Bon courage, " + nom + " !" + lineBreak);
        println("Si vous êtes prêt(e), appuyer sur " + ANSI_BOLD + "Entrée" + ANSI_RESET + " pour continuer.");
        String jouer = readString();
    }

    // affiche la partie entraînement du niveau
    void chestLevel(String coffre, int nbDeTour, Joueur player, CSVFile [] dataCsv, int niveauChest) {
        println(coffre); 
               
        println("Un coffre ! Ouvrez le afin de vous entraîner pour vos prochains combats.");
        println("Appuyer sur " + ANSI_BOLD + "Entrée" + ANSI_RESET + " pour continuer.");
        String jouer = readString();

        int num_matieres = choixMatieres(nbDeTour);
        
        boolean bonneReponse = poserQuestions(num_matieres, dataCsv, typeDeDifficulté(nbDeTour));
        
        if(bonneReponse == false){
            player.nbVies--;
            println(ANSI_CYAN + ANSI_BOLD + "Vous venez de perdre une vie  !! " + lineBreak + "Il vous en reste " + player.nbVies + "." + ANSI_RESET);  
            println("Encore " + (3-niveauChest-1) + " coffre(s).");
            println("Appuyer sur " + ANSI_BOLD + "Entrée" + ANSI_RESET + " pour continuer.");
            jouer = readString();
        }
        else{
            if(niveauChest == 3){
                println("Super ! Il n'y a plus de coffre.");
            }else{
            println(ANSI_GREEN + "Bien joué ! Encore " + (3-(niveauChest+1)) + " coffre(s)." + ANSI_RESET);
            }
        }
    }

    // affiche la partie "boss" à valider pour passer au niveau suivant
    
    void enemyLevel(String ennemi, CSVFile [] dataCsv, Joueur player, int nbDeTour) {
        println(ennemi); 
               
        println("Vous voilà face à un ennemi ! Répondez à toutes les questions correctement et vous le vaincrez.");
        println("Si vous êtes prêt(e) au combat, appuyer sur " + ANSI_BOLD + "Entrée" + ANSI_RESET + " pour continuer.");
        String jouer = readString();

        int num_matieres = choixMatieres(nbDeTour);

        boolean bonneReponse = poserQuestions(num_matieres, dataCsv, typeDeDifficulté(nbDeTour));

        if(bonneReponse == false){
            println("L'ennemi ricane et s'enfuit en vous laissant blessé.");
            player.nbVies--;
            println("Il vous reste " + ANSI_CYAN + ANSI_BOLD + player.nbVies + " vie(s)." + ANSI_RESET);
            if (player.nbVies >0) {
                println("Continuez de vous entraîner !");
            }
        } else {
            println(ANSI_GREEN + "Bravo " + player.nom + " ! Vous avez vaincu l'ennemi et vous passez à l'entraînement suivant." + ANSI_RESET+ lineBreak);
        }
    }
    

    void plusDeVie(int nbDeTour, String coffre, String restartContinue, String ennemi, String menu, String fin, String perso, CSVFile [] dataCsv, int start, CSVFile scoresCSV) {
        println(restartContinue);
        String choice = readString();

        while(!(equals(choice, "1")) && !(equals(choice, "2"))){
            println("Entrez une valeur correcte ! [1 ou 2]");
            choice = readString();
        }
        if(equals(choice, "1")){
            start = 2;
        }else{
            game(nbDeTour, coffre, restartContinue, ennemi, menu, fin, perso, dataCsv, scoresCSV);
        }
    }
    
    // avoir un types de difficultés aléatoire
    int typeDeDifficulté(int nbDeTour){
        int difficulty = 0;
        for(int i = 0; i <= 18; i = i + 6){
            if(nbDeTour >= i && nbDeTour <= 6+i ){
                difficulty = (int) (random()* (6+i));
                // pour ne pas avoir un difficulté de 0.
                while(difficulty > 18) {
                    difficulty = (int) (random()*7);
                }
                while(difficulty == 0){
                    difficulty = (int) (random()* (7+i));
                }
            }
            if(nbDeTour == 6+i){
                difficulty = 6+i;
            }
        }
        println("-- La difficulté est de " + difficulty + " --");

        return difficulty;

    }
    void game(int nbDeTour, String coffre,String restartContinue, String ennemi, String menu, String fin, String perso, CSVFile [] dataCsv, CSVFile scoresCSV){
        nbDeTour = 0;
        clearScreen();
        // afficher le menu principal (choix quitter ou nouvelle partie)
        println(menu);

        int start = startGame();
        Joueur player = creerJoueur();


        while(start != 2  && player.nbVies > 0 && nbDeTour != 19){
            
            int niveauChest = 0;

            //println(typeDeDifficulté(nbDeTour));
            // crée un joueur
            
            if (nbDeTour == 0) {
                player.nom = ANSI_BLUE + pseudoJoueur(player) + ANSI_RESET;
                println(lineBreak + "C'est parti, " + player.nom + " !" + lineBreak); 

                welcome(perso, player.nom);
            }
            
            
            while(start != 2  && player.nbVies > 0 && nbDeTour != 19){

                if(nbDeTour+1 == 20) {
                    println("Vous avez complété le jeu. Bravo !");
                    println(player.nom + ", vous êtes plein(e) de connaissances à présent." + lineBreak + "Faites en bon usage !");
                    start = 2;
                }
            
                if(start != 2  && player.nbVies > 0 && nbDeTour != 19)
                while(niveauChest < 3){
                    if(start == 2  || player.nbVies < 1 || nbDeTour == 19){
                        niveauChest = 3;
                    }
                    println(lineBreak + ANSI_BOLD + "Niveau " + (nbDeTour+1) + ANSI_RESET);

                    chestLevel(coffre, nbDeTour, player, dataCsv, niveauChest);
                    niveauChest++;
                    nbDeTour++;
                }

                if(player.nbVies < 1){
                    start = 2;                        
                }else{
                    println("Vous possédez : " + ANSI_CYAN + ANSI_BOLD + player.nbVies + " vie(s)." + ANSI_RESET + ANSI_RESET + lineBreak);
                    niveauChest = 0;
                    println(lineBreak + ANSI_BOLD + "Niveau " + (nbDeTour+1) + ANSI_RESET);
                    enemyLevel(ennemi, dataCsv, player, nbDeTour);
                    nbDeTour++;
                }
            }
        }
        if (start != 2 || player.nbVies == 0) {
            plusDeVie(nbDeTour, coffre, restartContinue , ennemi, menu, fin, perso, dataCsv, start, scoresCSV);
            updateScore(player.nom, nbDeTour, scoresCSV);
        }

        println(fin);
        println(afficherScore("scores"));
    }

// afficher le tableau des scores passés
    String afficherScore (String nom) {
        String pseudo = "";
        String score = "";
        String res = "";
        
        CSVFile scoresCSV = loadCSV("scores.csv");
        for (int cpt = 0; cpt < rowCount(scoresCSV); cpt ++) {
            pseudo = getCell(scoresCSV, cpt, 0);
            score = getCell(scoresCSV, cpt, 1);
            res = res + pseudo + " - " + score + lineBreak;
        }
        return res;
    }

// ajouter le score réalisé au tableau des scores
    void updateScore (String pseudo, int nbDeTour, CSVFile scores) {

        String scoreJoueur = "" + nbDeTour;
        CSVFile scoresCSV = loadCSV("scores.csv");
        String [] pseudoPartie = new String [] {pseudo};
        String [] scorePartie = new String [] {scoreJoueur};
        String [][] newScore = new String [rowCount(scoresCSV)+2][columnCount(scoresCSV)+2];

        for (int lig = 0; lig < rowCount(scoresCSV); lig++) {
            for (int col = 0; col < columnCount(scoresCSV); col++) {
                String cellPseudo = getCell(scoresCSV, lig, 0);
                String cellScore = getCell(scoresCSV, lig, 1);
                newScore[lig][col] = cellPseudo + "," + cellScore;
            }
        }
        newScore[rowCount(scoresCSV)+1][columnCount(scoresCSV)+1] = pseudoPartie[0] + "," + scorePartie[0];
        saveCSV(newScore, "scores.csv");
    }

    
// -----------------------------------------------------------------------------------------------
    
    // programme principal
   void algorithm(){
    // charger les données
        CSVFile [] dataCsv = chargerFichiersCsv("data");
        
    // afficher le tableau des scores
        CSVFile scoresCSV = loadCSV("scores.csv");
        println(afficherScore("scores"));
        
        String restartContinue = chargerFichierTexte("ressources", "restartContinue.txt");
        String coffre = chargerFichierTexte("ressources", "coffre.txt");
        String ennemi = chargerFichierTexte("ressources", "méchant.txt");
        String menu = chargerFichierTexte("ressources", "menu.txt");
        String fin = chargerFichierTexte("ressources", "fin.txt");
        String perso = chargerFichierTexte("ressources", "perso.txt");
        
        // afficher le menu principal (choix quitter ou nouvelle partie)
        println(menu);

        int start = startGame();
        Joueur player = creerJoueur();


        while(start != 2  && player.nbVies > 0 && nbDeTour != 19){
            
            int niveauChest = 0;

            //println(typeDeDifficulté(nbDeTour));
            // crée un joueur
            
            if (nbDeTour == 0) {
                player.nom = ANSI_BLUE + pseudoJoueur(player) + ANSI_RESET;
                println(lineBreak + "C'est parti, " + player.nom + " !" + lineBreak); 

                welcome(perso, player.nom);
            }
            
            
            while(start != 2  && player.nbVies > 0 && nbDeTour != 19){

                if(nbDeTour+1 == 20) {
                    println("Vous avez complété le jeu. Bravo !");
                    println(player.nom + ", vous êtes plein(e) de connaissances à présent." + lineBreak + "Faites en bon usage !");
                    start = 2;
                }
            
                if(start != 2  && player.nbVies > 0 && nbDeTour != 19)
                while(niveauChest < 3){
                    if(start == 2  || player.nbVies < 1 || nbDeTour == 19){
                        niveauChest = 3;
                    }
                    println(lineBreak + ANSI_BOLD + "Niveau " + (nbDeTour+1) + ANSI_RESET);

                    chestLevel(coffre, nbDeTour, player, dataCsv, niveauChest);
                    niveauChest++;
                    nbDeTour++;
                }

                if(player.nbVies < 1){
                    start = 2;                        
                }else{
                    println("Vous possédez : " + ANSI_CYAN + ANSI_BOLD + player.nbVies + " vie(s)." + ANSI_RESET + ANSI_RESET + lineBreak);
                    niveauChest = 0;
                    println(lineBreak + ANSI_BOLD + "Niveau " + (nbDeTour+1) + ANSI_RESET);
                    enemyLevel(ennemi, dataCsv, player, nbDeTour);
                    nbDeTour++;
                }
            }
        }
        if (start != 2 || player.nbVies == 0) {
            plusDeVie(nbDeTour, coffre, restartContinue , ennemi, menu, fin, perso, dataCsv, start, scoresCSV);
            updateScore(player.nom, nbDeTour, scoresCSV);
        }

        println(fin);
        println(afficherScore("scores"));
    }
}
